#!/bin/bash

set -e
set -x

# /dev/sdb is a disk (device) name
# n   add a new partition
# p   print the partition table
# 1   choose partition No. 1
# t   change a partition type
# 8e  means a LVM partition
# w   write table to disk and exit
sudo fdisk -u /dev/sdb <<EOF
n
p
1


t
8e
w
EOF

# /dev/sdb1 is a partition name
pvcreate /dev/sdb1
vgextend vagrant-vg /dev/sdb1
lvextend /dev/vagrant-vg/root /dev/sdb1
resize2fs /dev/vagrant-vg/root
