#!/bin/bash

function main() {   
  local -r user='vagrant';
  
  pushd /vagrant/install
  ./configure-apt
  
  ./install-xfce
  ./install-guest-additions "${user}"
  ./install-packet-installation-tools 
  ./install-git
  ../setupstream "https://bitbucket.org/mateuszkucharczyk/toolbox.git" "/vagrant"
  ./install-webbrowser
  ./install-packet-dev "${user}"
  popd;
}

main "$@" > /vagrant/stdout-dev.log
